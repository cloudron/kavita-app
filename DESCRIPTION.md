### About

Kavita is a fast, feature rich, cross platform reading server. Built with a focus for manga and the goal of being a full solution for all your reading needs. Setup your own server and share your reading collection with your friends and family.

### Features

* Serve up Manga/Webtoons/Comics (cbr, cbz, zip/rar/rar5, 7zip, raw images) and Books (epub, pdf)
* First class responsive readers that work great on any device (phone, tablet, desktop)
* Dark mode and customizable theming support
* External metadata integration and scrobbling for read status, ratings, and reviews (available via Kavita+)
* Rich Metadata support with filtering and searching
* Ways to group reading material: Collections, Reading Lists (CBL Import), Want to Read
* Ability to manage users with rich Role-based management for age restrictions, abilities within the app, etc
* Rich web readers supporting webtoon, continuous reading mode (continue without leaving the reader), virtual pages (epub), etc
* Full Localization Support
* Ability to customize your dashboard and side nav with smart filters, custom order and visibility toggles.

