# Kavita Cloudron App

This repository contains the Cloudron app package source for [Kavita](https://www.kavitareader.com/).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=org.kavita.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id org.kavita.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd kavita-app

cloudron build
cloudron install
```
