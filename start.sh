#!/bin/bash

set -eu

readonly sqlite="gosu cloudron:cloudron sqlite3 /app/data/config/kavita.db"

mkdir -p /app/data/{manga,books,comics} /run/kavita

[[ ! -d /app/data/config ]] && cp -r /app/code/config.orig /app/data/config
cp -r /app/code/wwwroot.orig /run/kavita/wwwroot

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data /run/kavita

wait_for_kavita() {
    # Wait for app to come up
    while ! curl --fail -s http://localhost:5000 > /dev/null; do
        echo "=> Waiting for app to come up"
        sleep 3
    done
}

# sqlite3 does not allow multiple writes in WAL mode. This means that we cannot update settings in the background
# https://www.sqlite.org/wal.html (concurrency section, 3rd para)
if [[ ! -f "/app/data/config/kavita.db" ]]; then
    echo "==> Creating database on first run"
    /usr/local/bin/gosu cloudron:cloudron /app/code/Kavita &
    pid=$!
    sleep 5

    ( wait_for_kavita )

    # https://wiki.kavitareader.com/en/faq#q-im-seeing-database-is-locked-errors-in-my-logs
    $sqlite3 "PRAGMA journal_mode=WAL;"

    echo "==> Stopping after database creating"
    kill -SIGTERM ${pid} # this kills the process group
fi

echo "=> Updating server settings"
# RowVersion is a concurrency token
# Key is enum from https://github.com/Kareadita/Kavita/blob/master/API/Entities/Enums/ServerSettingKey.cs#L9
# Hostname is the origin - https://github.com/Kareadita/Kavita/issues/2878
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (20, 2, \"https://${CLOUDRON_APP_DOMAIN}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (28, 2, \"${CLOUDRON_MAIL_FROM}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (29, 2, \"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Kavita}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (30, 2, \"${CLOUDRON_MAIL_SMTP_USERNAME}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (31, 2, \"${CLOUDRON_MAIL_SMTP_PASSWORD}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (32, 2, \"${CLOUDRON_MAIL_SMTP_SERVER}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (33, 4, \"${CLOUDRON_MAIL_SMTP_PORT}\")"
$sqlite "INSERT OR REPLACE INTO ServerSetting (Key, RowVersion, Value) VALUES (34, 4, \"False\")"

echo "=> Starting Kavita"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/Kavita
