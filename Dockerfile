FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg/
WORKDIR /app/code

ENV DOTNET_RUNNING_IN_CONTAINER=true

# renovate: datasource=github-releases depName=Kareadita/Kavita versioning=loose extractVersion=^v(?<version>.+)$
ARG KAVITA_VERSION=0.8.5

RUN curl -L https://github.com/Kareadita/Kavita/releases/download/v${KAVITA_VERSION}/kavita-linux-x64.tar.gz | tar xz --strip-components 1 -C /app/code

RUN chmod +x /app/code/Kavita && \
    mv /app/code/config /app/code/config.orig && ln -sf /app/data/config /app/code/config && \
    mv /app/code/wwwroot /app/code/wwwroot.orig && ln -sf /run/kavita/wwwroot /app/code/wwwroot && \
    chown -R cloudron:cloudron /app/code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
