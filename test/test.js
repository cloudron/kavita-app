#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const ADMIN_USERNAME = "admin";
    const ADMIN_EMAIL = "admin@cloudron.local";
    const ADMIN_PASSWORD = "changeme123";

    const LIB_TITLE = 'Cloudron Library';
    const LIB_PATH = '/app/data/books';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerAdmin() {
        await browser.get(`https://${app.fqdn}/registration/register`);
        await waitForElement(By.id('username'));

        await browser.findElement(By.id('username')).sendKeys(ADMIN_USERNAME);
        await browser.findElement(By.id('email')).sendKeys(ADMIN_EMAIL);
        await browser.findElement(By.id('password')).sendKeys(ADMIN_PASSWORD);
        await browser.sleep(3000);
        await browser.findElement(By.xpath('//button[text()="Register"]')).click();

        await waitForElement(By.xpath('//button[contains(., "Sign in")]'));
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/login`);

        await waitForElement(By.id('username'));

        await browser.findElement(By.id('username')).sendKeys(username);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).submit();

        await waitForElement(By.xpath('//button[contains(., "' + username.charAt(0).toUpperCase() + username.slice(1) + '")]'));
    }

    async function logout(username) {
        await browser.get(`https://${app.fqdn}`);

        await waitForElement(By.xpath('//button[contains(., "' + username.charAt(0).toUpperCase() + username.slice(1) + '")]'));
        await browser.findElement(By.xpath('//button[contains(., "' + username.charAt(0).toUpperCase() + username.slice(1) + '")]')).click();

        await waitForElement(By.xpath('//a[text()="Logout"]'));
        await browser.findElement(By.xpath('//a[text()="Logout"]')).click();

        await waitForElement(By.xpath('//button[@type="submit"]'));
    }


    async function createLibrary() {
        await browser.get(`https://${app.fqdn}/settings#admin-libraries`);

        await waitForElement(By.xpath('//button[@title="Add Library"]'));
        await browser.findElement(By.xpath('//button[@title="Add Library"]')).click();

        await waitForElement(By.id('library-name'));
        await browser.findElement(By.id('library-name')).sendKeys(LIB_TITLE);
        await browser.findElement(By.id('library-type')).click();
        await browser.findElement(By.xpath('//option[text()="Book"]')).click();

        await waitForElement(By.xpath('//button[text()="Next"]'));
        await browser.findElement(By.xpath('//button[text()="Next"]')).click();

        await waitForElement(By.xpath('//button[contains(., "Browse for Media Folders")]'));
        await browser.findElement(By.xpath('//button[contains(., "Browse for Media Folders")]')).click();

        await waitForElement(By.xpath('//input[@placeholder="Start typing or select path"]'));
        await browser.findElement(By.xpath('//input[@placeholder="Start typing or select path"]')).sendKeys(LIB_PATH);

        await waitForElement(By.xpath('//button[text()="Share"]'));
        await browser.findElement(By.xpath('//button[text()="Share"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Next"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Next"]')).click();
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();

        await waitForElement(By.xpath(`//a[text()="${LIB_TITLE}"]`));
    }

    async function checkLibrary() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[contains(@href, "library")]//div[contains(text(), "' + LIB_TITLE + '")]'));
    }

    async function uploadBook() {
        execSync(`cloudron push --app ${app.id} pdf-book ${LIB_PATH}`);
        execSync(`cloudron exec --app ${app.id} chown cloudron:users ${LIB_PATH}/pdf-book`, EXEC_ARGS);
    }

    async function checkBook() {
        checkLibrary();

        await waitForElement(By.id('pdf-book_0'));
        await browser.findElement(By.id('pdf-book_0')).click();

        await waitForElement(By.xpath('//h4/span[text()="pdf-book "]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    it('can install app', async function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(5000);
    });
    it('can get app information', getAppInfo);

    it('can upload book', uploadBook);
    it('can admin register', registerAdmin);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can create library', createLibrary);
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('can restart app', async function () {
        execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(5000);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));


    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.kavitareader.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can upload book', uploadBook);

    it('can admin register', registerAdmin);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can create library', createLibrary);
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('can update', async function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
    });

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can check library', checkLibrary);
    it('can check book', checkBook);
    it('can admin logout', logout.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

});
